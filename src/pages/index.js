import React, { useState } from "react";
import ky from "ky";
import { useQuery } from "react-query";
import "../css/index.css";

const getImages = async (page = 0) => {
  const images = await ky(
    `http://localhost:8080/api/photos?limit=4&page=${page}`
  ).json();
  return images;
};

const IndexPage = () => {
  const [page, setPage] = React.useState(0);
  const [selection, updateSelection] = useState(null);

  const update = (image) => {
    updateSelection(image);
  };

  const query = useQuery(["images", page], () => getImages(page), {
    keepPreviousData: true,
  });
  return (
    <main id="container">
      <div className="group" id="large">
        {selection && (
          <>
            <img src={`http://localhost:8080/large/${selection.image}`} />
            <div className="details">
              <strong>{selection.title}</strong>
              <p>{selection.description}</p>
              <p>${selection.cost}</p>
            </div>
          </>
        )}
      </div>
      <div className="thumbnails group">
        <div
          className={`previous ${page === 0 ? "disabled" : ""}`}
          onClick={() => setPage((old) => Math.max(old - 1, 0))}
          disabled={page === 0}
        />
        {query.isSuccess &&
          query.data.results.map((image) => (
            <a href="#" onClick={() => update(image)}>
              <img
                src={`http://localhost:8080/thumbnails/${image.thumbnail}`}
              />
              <span>{image.thumbnail}</span>
            </a>
          ))}
        <div
          className={`next ${
            query.isPreviousData || !query.data?.hasMore ? "disabled" : ""
          }`}
          onClick={() => {
            if (!query.isPreviousData && query.data.hasMore) {
              setPage((old) => old + 1);
            }
          }}
          disabled={query.isPreviousData || !query.data?.hasMore}
        />
      </div>
    </main>
  );
};

export default IndexPage;
